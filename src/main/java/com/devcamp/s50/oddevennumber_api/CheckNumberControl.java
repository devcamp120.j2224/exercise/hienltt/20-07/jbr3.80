package com.devcamp.s50.oddevennumber_api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckNumberControl {
    @CrossOrigin
    @GetMapping("/checknumber")
    public String getNumber(){
        String thongbao = "";

        //Khai báo đầu vào request
        int number = 99;

        if(number % 2 == 0) {
            thongbao = "This number is EVEN";
        } else {
            thongbao = "This number is ODD";
        }
        System.out.println(thongbao);
        return thongbao;
    }
}
